<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'stokoe');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1c)ieO~-=O3=q4ZEBIU$#MMH!N4(`!&6!5z,%SZzBIFA4> ra]d!dkPF^ !eFT:;');
define('SECURE_AUTH_KEY',  'S6-k}+d#`UNdE^>J nQOdCAeHxm,c;Z5)J+{gL&!DOAZ<4JJ81+:1?YCg+sxNwJ+');
define('LOGGED_IN_KEY',    '*p__vzN3 @Dh8T<t/gg4HwkEW{v@u0D ? N{XiM)::K8_k:Jk5xWZ3kb|*Qh$s,}');
define('NONCE_KEY',        'ntHcKR-EQCP/J;!U%v&L~ovL{m!pgRqt4bPH+v$EvUsI7G<X8MeOfO9A0u{f.QAQ');
define('AUTH_SALT',        'pvu^qp?+idG75+1e#!#b](L^mt**(%>Sl~clPlI(9bVa=|nBDb*FD+>|juL2s1}e');
define('SECURE_AUTH_SALT', ';g#BaAPH3j,k%1A4-}koTbOT.0kX>GTDF%5@$WBJLq xF?Lkv7W1y g.(;)H7-vY');
define('LOGGED_IN_SALT',   'A*v+6#&hM:`rP9z&5UK flAJ6%+V% L4,U@6dtVa]j.~rrg;oP^dzI;x5[s|;^;}');
define('NONCE_SALT',       '.6=o!a 4t=(fnWq<H~GpXP4pD_qn4[X<B)?%b24.NEmP!D(NgKn%vJ)4!AcD!$}J');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'st_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
