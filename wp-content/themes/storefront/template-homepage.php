<?php
/**
 * The template for displaying the homepage.
 *
 * This page template will display any functions hooked into the `homepage` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Homepage
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/**
			 * Functions hooked in to homepage action
			 *
			 * @hooked storefront_homepage_content      - 10
			 */
			do_action( 'homepage' ); ?>

			<div class="seasons"><?php the_field('seasons'); ?></div>
			<div class="middle">
				<div class="inner">
					<div class="left">
						<div class="opening">Opening Day - <?php the_field('middle_content'); ?></div>
						<div class="insta">
							<h1>Whats Happpening at Stokoe</h1>
							<div class="feed"><?php the_field('instagram'); ?></div>
						</div>
					</div>
					<div class="basic"><?php the_field('basic_information'); ?></div>
				</div>	
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
