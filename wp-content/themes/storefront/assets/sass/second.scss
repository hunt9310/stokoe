body{
 #page{
  .site-content{
   #second{
      position: relative;
      margin-top: 0;
      margin-bottom: -50px;
      width: 100%;
      .top{
       position: relative;
       top: -35vh;
       margin-bottom: -35vh;
       height: 30vh;
       overflow: hidden;
       img{
        top: -200px;
        width: 100%;
        position: relative;
       }
      }
      #main{
       display: inline-block;
       width: 100%;
       background: url(../storefront/assets/images/Wood_bkgrd.jpg);
       background-size: 100% 110%;
       background-position: 0px;
       box-shadow: 0px 0 30px rgba(0, 0, 0, 1);
       position: relative;
       .inner{
        display: block;
        width: 95%;
        max-width: 1000px;
        margin: 0 auto; 
        position: relative;  
        padding: 75px 0;  
        &.full{
         .page{
          width: 100%;
         }
        }
        .sidebar{
         display: inline-block;
         z-index: 10;
         width: 25%;
        position: relative;
        padding: 25px 15px;
        color: white;
        font-family: helvetica;
        font-weight: bold;
        font-size: 10pt;
         background: url(../storefront/assets/images/light_bg.png);
         background-size: 100% 100%;   
         text-shadow: 2px 2px 1px rgba(0,0,0,.75);    
         font-family: Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif;
         ul{
          list-style: none;
          padding: 0;
          margin: 0; 
          li{
           border-top: 1px solid rgba(0,00,0,.2);
           border-bottom: 1px solid white;
           position: relative;
           display: inline-block;
           width: 100%;
           &.current_page_item{
            background: rgba(0,0,0,.35);
           }
           &:first-of-type{
            border-top: none;
           }
           &:last-of-type{
            border-bottom: none;
           }
           &:hover{
            a{
             background: #3c2909;
             color: white;
             text-decoration: none;
            }
           }
           a{
            text-shadow: none;
            color: #3c2909;
            width: 100%;
            padding: 5px 10px;
            display: inline-block;
           }
           &:hover{
            .sub-menu{
             display: inline-block;
            }
           }
           .sub-menu{
            position: absolute;
            background: #3c2909;
            right: -100%;
            width: 100%;
            top: 0;
            display: none;
            padding: 15px; 
            li{
             a{
              &:hover{
               background: #de782a;
              }
             }
            }
           }
          }
         }
        }   
        .page{
         display: inline-block;
         width: 70%;
         float: right;
       position: relative;
         background: #eae5d8;
         height: auto;
         margin: 0 auto 50px;
         padding: 25px 25px 20px;
         box-shadow: 2px 2px 13px rgba(0, 0, 0, 0.65);
        &:before{  
          content: '';
          display: inline-block;
          position: absolute;
          top: 15px;
          left: 15px;
          width: 8px;
          height: 8px;
          background: #2e2f2d;
          border-radius: 50%;         
        }
        &:after{  
          content: '';
          display: inline-block;
          position: absolute;
          top: 15px;
          right: 15px;
          width: 8px;
          height: 8px;
          background: #2e2f2d;
          border-radius: 50%;         
        } 
        h1{
         font-family: 'Simonetta', cursive;
         font-weight: 400;
         color: #b22348;
         margin-bottom: 15px;
         font-size: 23pt;
         text-shadow: 1px 1px 1px rgba(0,0,0,.4);
         max-width: 55%;
        } 
       h2{
         font-family: 'Simonetta', cursive;
         font-weight: 400;
         color: #333;
         font-size: 18pt;
         margin-bottom: 25px;
        }  
        h3{
         font-family: 'Simonetta', cursive;
         font-weight: 800;
         font-size: 20px;
         color: #b22348;
         margin-bottom: 15px;
        } 
        h4{
         font-family: 'Simonetta', cursive;
         font-weight: 800;
         font-size: 18px;
         color: #b22348;
         margin-bottom: 15px;
        } 
        p{
        	text-align: justify;
          font-family: Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif;
          .alignnone{
          	display: inline-block;
          }
        	.main{
        		display: flex;
				    width: 35%;
				    float: right;
				    vertical-align: top;
				    padding: 5px; 
				    margin: -17% 10px 20px 20px;
				    border: 1px solid rgba(0,0,0,.15);
				    background: white;
				    box-shadow: 1px 1px 10px rgba(0,0,0,.25);
				    &.history{
				    	width: 50%;
				    	margin-top: -6%;
				    }
        	}
         strong{
          a{
           font-size: 14pt;
           font-family: 'Simonetta', cursive;
          }
         }
        }
        .calendar{
        	border: 1px solid rgba(0,0,0,.25);
        	.cal_h1{
	         font-family: 'Simonetta', cursive;
	         font-weight: 800;
	         font-size: 20px;
	         color: #b22348;
	         margin-bottom: 15px;
	        }
	        tr{
	        	&:nth-of-type(2){
	        		th{
	        			border-left: 1px solid rgba(0,0,0, .05);
				        font-family: 'Simonetta', cursive;
				        font-weight: 800;
				        font-size: 14px;
				        color: rgba(0,0,0,.45);
				        margin-bottom: 15px;	        			
	        		}
	        	}
	        	th{
	        		border-bottom: 1px solid rgba(0,0,0,.15);
	        		text-align: center;
	        	}
	        	td{
	        		border-right: 1px solid rgba(0,0,0,.15);
	        		border-bottom: 1px solid rgba(0,0,0,.15);
	        		font-family: Palatino, "Palatino Linotype", "Palatino LT STD", "Book Antiqua", Georgia, serif;
	        		font-weight: bold;
	        		text-align: left;
	        		.calendar_text{
								font-family: 'Simonetta', cursive;
								font-weight: bold;
								margin-bottom: 5px;
								padding-bottom: 5px;
								width: 100%;
								border-bottom: 1px solid rgba(0,0,0,.15); 
								display: inline-block;
								font-size: 9pt;
	        		}
	        		a{
								color: white;
								text-decoration: underline;
								font-size: 9pt;
								font-weight: bold;
								line-height: 14px;
								display: inline-block;
								font-family: 'Simonetta', cursive;
								margin-bottom: 10px;
								text-align: left;
	        		}
	        	}
	        }
	      }
        .discount{
         ul{
          font-size: 13pt;
          list-style: none;
          li{
          	border-bottom: 1px solid rgba(0,0,0,.05);
          	padding-bottom: 5px;
          	margin-bottom: 5px;
          	&:last-of-type{
          		border-bottom: none;
          	}
          }
         }
         h5{
          font-size: 13pt;
         }
        }
        .harvest{
         h3{
          margin-bottom: 30px;
         } 
         ul{
          list-style: none;
          li{
           border-bottom: 1px solid rgba(0,0,0,.05);
           padding-bottom: 5px;
           margin-bottom: 15px;
           &:last-of-type{
            border-bottom: none;
           }
          }
          h4{
           font-family: 'Simonetta', cursive;
           font-weight: 800;
           font-size: 16px;
           color: #b22348;
           margin-bottom: 5px;
          } 
         }
        }
        .events{
         ul{
          list-style: none;
          margin: 0;
          padding: 0;
          li{
           padding: 10px 0;
          }
         }
        }
        .products{
         list-style: none;
         li{
          display: inline-block;
          width: 100%;
          text-align: left;
          padding-bottom: 10px;
          margin-bottom: 10px;
          border-top: 1px solid rgba(0,0,0,.15);
          .woocommerce-LoopProduct-link{
           width: 20%;
           vertical-align: top;
           display: inline-block;
           img{
            width: 100%;
            display: inline-block;
            margin-bottom: 0;
            position: relative;
            top: -1px;
           }
          }
          .info-wrap{
           display: inline-block;
           width: 80%;
           height: 190px;
           position: relative;
           h2{
            position: absolute;
            top: 20px;
            left: 20px;
            font-size: 16pt;
           font-family: 'Simonetta', cursive;
           font-weight: 400;
           color: #b22348;
           }
           .woocommerce-product-details__short-description{
           display: inline-block;
           min-height: auto;
           text-align: left;
           padding: 15px;
           font-size: 10pt;
           line-height: 22px;
           width: 70%;
           margin: 0 -3px 10px;
           margin: 55px 0px 0 20px;
           background: rgba(255, 255, 255, 0.5);
           padding: 10px;
           border: 1px solid rgba(0, 0, 0, 0.05);          
           strong{
            color: #b22348;
           }
            ul{
             list-style: disc;
             display: list-item;
             li{
              border-top: none;
              display: list-item;
              padding: 0;
              margin: 0;
              font-weight: bold;
             }
            }
           }           
          }
          .price{
           display: inline-block;
           width: 25%;
           vertical-align: top;
           background:  #b22348;
           text-align: center;
           padding: 25px 0;
           height: 50%;
           float: right;
           .woocommerce-Price-amount{
            font-size: 18pt;
           font-family: 'Simonetta', cursive;
           font-weight: 800;
           color: white;
            &:after{
             content: 'Per Person';
             display: block;
            font-family: 'Simonetta', cursive;
            font-weight: 400;
            margin-top: -10px;  
            font-size: 10pt;
            color: white;
            }
           }
          }
          .button{
           display: inline-block;
           position: absolute;
           right: 0;
           left: 80%;
           top: 45%;
           text-transform: uppercase;
           border: 2px solid #b22348;
           background: transparent;
           color: #b22348;
           text-align: center;
          }
         }
        }
        .calendar_main{
         .ecwd_calendar{
          .calendar-head{
           background: transparent;
           .previous{
            border: none;
            a{
             position: relative;
              display: inline;
              font-family: 'Simonetta', cursive;
              font-weight: 800;
              color: #b22348;
              margin-bottom: 15px;
              opacity: .75;
              &:hover{
               opacity: 1;
               &:before{
                margin-right: 5px;
                left: -5px;
               }
              }
              &:before{
               content: "\e079";
               display: inline-block;
               font-family: 'Glyphicons Halflings';
               font-size: 9pt;   
               left: 0;
               position: relative;
               -webkit-transition: .15s;  
               -o-transition: .15s;  
               -ms-transition: .15s;  
               -moz-transition: .15s;         
              }
             span{
              &:first-of-type{
               display: none;
              }
             }
            }
           }
           .current-month{
            border: none;
             font-family: 'Simonetta', cursive;
             font-weight: 800;
             color: #333;
             font-size: 11pt;
             position: relative;
             div{
              font-size: 18pt;
             } 
             a{
              text-indent: -9999px;
              display: inline-block;
              &:first-of-type{
               &:before{
                content: "\e079";
                display: inline-block;
                text-indent: 0;
                position: absolute;
                left: 20%;
                top: 30px;
                font-family: 'Glyphicons Halflings';
                font-size: 13pt;  
                color: #b22348;
	              -webkit-transition: .15s;  
                -o-transition: .15s;  
                -ms-transition: .15s;  
                -moz-transition: .15s; 
               }
             }
              &:last-of-type{
               &:before{
                content: "\e080";
                display: inline-block;
                text-indent: 0;
                position: absolute;
                right: 20%;
                top: 30px;
                font-family: 'Glyphicons Halflings';
                font-size: 13pt;   
                color: #b22348;
              -webkit-transition: .15s;  
                -o-transition: .15s;  
                -ms-transition: .15s;  
                -moz-transition: .15s; 
               }
             }             
             }
           }
           .next{
            border: none;
            a{
             position: relative;
              display: inline;
              font-family: 'Simonetta', cursive;
              font-weight: 800;
              color: #b22348;
              margin-bottom: 15px;
              opacity: .75;
              &:hover{
               opacity: 1;
               &:after{
                margin-left: 5px;
                right: -5px;
               }
              }              
              &:after{
               content: "\e080";
               display: inline-block;
               position: relative;
               right: 0;
               font-family: 'Glyphicons Halflings';
               font-size: 9pt;           
             -webkit-transition: .15s;  
               -o-transition: .15s;  
               -ms-transition: .15s;  
               -moz-transition: .15s; 
              }
             span{
              &:last-of-type{
               display: none;
              }
             }
            }            
           }
          }
         }
        }              
        }
       }
     }
   }
  }
 }
}
#second {
	.master-slider-parent{    
		max-width: 250px;
    max-height: 150px;
    position: relative;
    display: inline-flex;
    float: right;
    vertical-align: top;
    margin-top: -15%;
    margin-right: 30px;
    padding: 0 0 20px 20px;
    img{
      width: 100% !important;
      margin: 0 !important;
    }
  }
}

.page-id-85{
 .page{
  ul{
   list-style: none;
   padding: 0;
   margin: 0;
   li{
    border-bottom: 1px solid rgba(00,00,0,0.15);
    padding: 15px 0 15px 10px;
    img{
    	display: inline-block;
    	width: 48%;
    	margin: 25px 0;
    	padding: 10px;
    	background: white; 
      box-shadow: 0px 0 30px rgba(0, 0, 0, .15);
      border: 1px solid rgba(0,0,0,.25);
    }
    &:last-of-type{
     border-bottom: none;
    }
    h4{
     margin-bottom: 0px !important;
    }
   }
  }
 }
}
.page-id-90{
	ul{
		li{
	    img{
	    	display: inline-block;
	    	margin: 0 25px 25px 0;
	    	padding: 10px;
	    	background: white; 
	      box-shadow: 0px 0 30px rgba(0, 0, 0, .15);
	      border: 1px solid rgba(0,0,0,.25);
	    }
		}
	}
}