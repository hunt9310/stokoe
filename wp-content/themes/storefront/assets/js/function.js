jQuery(document).ready( function() {
  (function ($) {
    $( document ).ready( function() {
      var now = new Date();
      var currentMonth = now.getMonth();//returns 0-11
      //based on month hide or show your div
      if( currentMonth > 0 && currentMonth < 3 ) {
        $( "#summer-panel").hide();
        $( "#winter-panel").show();
      } 
      else {
        $( "#summer-panel" ).show();
        $( "#winter-panel" ).hide();
      }
    });

    var toggle = $('.toggle');
    var nav = $('#page');

    $(toggle).click(function(){
        $(this).toggleClass('active');
        $(nav).toggleClass('active');
    });

    var arrow = $('body #page .site-header .navigation_inner .storefront-primary-navigation #site-navigation .primary-navigation ul li.menu-item-has-children');

    $(arrow).click(function(){
      $(this).toggleClass('active');
    });

  })(jQuery);
});