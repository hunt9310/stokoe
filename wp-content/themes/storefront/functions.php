<?php
/**
 * Storefront engine room
 *
 * @package storefront
 */

/**
 * Assign the Storefront version to a var
 */
$theme              = wp_get_theme( 'storefront' );
$storefront_version = $theme['Version'];

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 980; /* pixels */
}

$storefront = (object) array(
	'version' => $storefront_version,

	/**
	 * Initialize all the things.
	 */
	'main'       => require 'inc/class-storefront.php',
	'customizer' => require 'inc/customizer/class-storefront-customizer.php',
);

require 'inc/storefront-functions.php';
require 'inc/storefront-template-hooks.php';
require 'inc/storefront-template-functions.php';

if ( class_exists( 'Jetpack' ) ) {
	$storefront->jetpack = require 'inc/jetpack/class-storefront-jetpack.php';
}

if ( storefront_is_woocommerce_activated() ) {
	$storefront->woocommerce = require 'inc/woocommerce/class-storefront-woocommerce.php';

	require 'inc/woocommerce/storefront-woocommerce-template-hooks.php';
	require 'inc/woocommerce/storefront-woocommerce-template-functions.php';
}

if ( is_admin() ) {
	$storefront->admin = require 'inc/admin/class-storefront-admin.php';
}

wp_enqueue_script( 'function', get_template_directory_uri() . '/assets/js/function.js');

function wpgyan_widgets_init() {

	register_sidebar( array(
		'name' => 'Footer Menu',
		'id' => 'footer_menu',
		'before_widget' => '<div id="wpgyan-widget">',
		'after_widget' => '</div>',		
	) );
}
add_action( 'widgets_init', 'wpgyan_widgets_init' );

function pumpkin_widgets_init() {

	register_sidebar( array(
		'name' => 'Pumpkin Menu',
		'id' => 'pumpkin_menu',
		'before_widget' => '<div id="wpgyan-widget">',
		'after_widget' => '</div>',		
	) );
}
add_action( 'widgets_init', 'pumpkin_widgets_init' );

function tree_widgets_init() {

	register_sidebar( array(
		'name' => 'Tree Menu',
		'id' => 'tree_menu',
		'before_widget' => '<div id="wpgyan-widget">',
		'after_widget' => '</div>',		
	) );
}
add_action( 'widgets_init', 'tree_widgets_init' );

/**
 * Sets the default date for event queries.
 *
 * Expects to be called during tribe_events_pre_get_posts. Note that this
 * function modifies $_REQUEST - this is needed for consistency because
 * various parts of TEC inspect that array directly to determine the current
 * date.
 * 
 * @param WP_Query $query
 */
function tribe_force_event_date( WP_Query $query ) {
    // Don't touch single posts or queries other than the main query
    if ( ! $query->is_main_query() || is_single() ) {
        return;
    }

    // If a date has already been set by some other means, bail out
    if ( strlen( $query->get( 'eventDate' ) ) || ! empty( $_REQUEST['tribe-bar-date'] ) ) {
        return;
    }

    // Change this to whatever date you prefer
    $default_date = '2015-10-01';

    // Use the preferred default date
    $query->set( 'eventDate', $default_date );
    $query->set( 'start_date', $default_date );
    $_REQUEST['tribe-bar-date'] = $default_date;
}

add_action( 'tribe_events_pre_get_posts', 'tribe_force_event_date' ); 

/**
 * Note: Do not add any custom code here. Please use a custom plugin so that your customizations aren't lost during updates.
 * https://github.com/woocommerce/theme-customisations
 */
