<?php
/**
 * The template for displaying the Full Section.
 *
 * This page template will display any functions hooked into the `Full` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Full Section
 *
 * @package storefront
 */

get_header(); ?>

	<div id="second" class="content-area">
		<div class="top"><?php the_post_thumbnail(); ?></div>
		<main id="main" class="site-main" role="main">
			<div class="full inner">
				<?php
				/**
				 * Functions hooked in to secondpage action
				 *
				 * @hooked storefront_secondpage_content      - 10
				 */
				do_action( 'secondpage' ); ?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
