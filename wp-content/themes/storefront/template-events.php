<?php
/**
 * The template for displaying the Events Section.
 *
 * This page template will display any functions hooked into the `Events` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Events Section
 *
 * @package storefront
 */

get_header(); ?>

	<div id="second" class="content-area">
		<div class="top"><?php the_post_thumbnail(); ?></div>
		<main id="main" class="site-main" role="main">
			<div class="inner">
				<div class="sidebar"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('events_menu') ) : endif; ?></div>
				<?php
				/**
				 * Functions hooked in to secondpage action
				 *
				 * @hooked storefront_secondpage_content      - 10
				 */
				do_action( 'secondpage' ); ?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
