<?php
/**
 * The template for displaying the Tree Section.
 *
 * This page template will display any functions hooked into the `Tree` action.
 * By default this includes a variety of product displays and the page content itself. To change the order or toggle these components
 * use the Homepage Control plugin.
 * https://wordpress.org/plugins/homepage-control/
 *
 * Template name: Tree Section
 *
 * @package storefront
 */

get_header(); ?>

	<div id="second" class="content-area">
		<div class="top"><img src="http://armorsecurityandprotection.com/stokoe/wp-content/uploads/2017/06/Christmas-slider-1-red-tractors.jpg" alt="Tractor" /></div>
		<main id="main" class="site-main" role="main">
			<div class="inner">
				<div class="sidebar"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('tree_menu') ) : endif; ?></div>
				<?php
				/**
				 * Functions hooked in to secondpage action
				 *
				 * @hooked storefront_secondpage_content      - 10
				 */
				do_action( 'secondpage' ); ?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
